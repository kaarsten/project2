// First, import the classes for the board.
import boardgame.reversi.*;
import boardgame.*;

public class Reversi
{
    public static void main(String args[])
    {
        // Check for invalid number of arguments. If found, bail.
        if(args.length < 2)
        {
            System.out.println("You must say what kind of players to play with (and optionally,");
            System.out.println("a size, which defaults to 8 otherwise).");
            System.out.println("Example: \"Human RandomComputer\", \"Human Human\", \"RandomComputer IntelligentComputer\"");
            System.exit(1);
        }
        if(args.length > 3)
        {
            System.out.println("Too many arguments passed.");
            System.exit(1);
        }

        // Create an array to hold the player types. Based on the enum
        // of player types stored in the board class.
        ReversiBoard.PlayerType players[] = new ReversiBoard.PlayerType[2];
        
        // Now, set the default size.
        int size = 8;
        
        // Loop through the command line arguments. If a player type is invalid, notify.
        for(int argc = 0; argc < 2; argc++)
        {
            if(args[argc].toLowerCase().equals("human"))
                players[argc] = ReversiBoard.PlayerType.HUMAN;
            else if(args[argc].toLowerCase().equals("randomcomputer"))
                players[argc] = ReversiBoard.PlayerType.RANDOMCPU;
            else if(args[argc].toLowerCase().equals("intelligentcomputer"))
                players[argc] = ReversiBoard.PlayerType.SMARTCPU;
            else
            {
                System.out.println("Argument for player "+(argc+1)+"is invalid.");
                System.exit(1);
            }
        }
        
        // If we've got a third argument, it means we've got a custom board size.
        // If the string passed is not a number or an odd one, bail.
        if(args.length == 3)
            {
            try
            {
                size = Integer.parseInt(args[2]);
                if(size % 2 == 1 || size < 3)
                    throw new Exception("Bleh.");
            }
            catch(Exception e)
            {
                System.out.println("Size argument is invalid.");
                System.exit(1);
            }
        }

        // Alright, now create the new board with the given playertypes and
        // size.
        ReversiBoard gameBoard = new ReversiBoard(players[0],players[1],size);

        // Initialize the board loop. 'game' is the game state; as long as it is true, the game
        // loop will run. 'color' describes the player turn state. 'false' indicates the black
        // player's turn, 'true' indicates the white player's turn. 'prevpass' indicates if the
        // previous player was forced to pass.
        Boolean game = true;
        Boolean color = false;
        Boolean prevPass = false;
        
        // Now, get references to the black and white players themselves.
        Player black, white;
        black = gameBoard.getPlayer(ReversiBoard.PlayerColor.BLACK);
        white = gameBoard.getPlayer(ReversiBoard.PlayerColor.WHITE);

        // Game loop!
        while(game && gameBoard.getFreeSpaceCount() > 0)
        {
            // Print out the game board and current stats.
            System.out.println(gameBoard);
            
            // Check which player's turn it is and if that player has available plays.
            // IF the current player has a turn and no available plays, the game is over.
            // Otherwise, print which player's turn it is and call their play function.
            if(color)
            {
                if(gameBoard.hasAvailablePlays(white))
                {
                    if(prevPass)
                        prevPass = false;
                    System.out.println("It is the white player's turn.");
                    white.play();
                }else{
                    if(!prevPass)
                        prevPass = true;
                    else
                        game = false;
                    System.out.println("White has no valid moves, forced to pass...");
                }
            }else if(!color)
            {
                if(gameBoard.hasAvailablePlays(black))
                {
                    if(prevPass)
                        prevPass = false;
                    System.out.println("It is the black player's turn.");
                    black.play();
                }else{
                    if(!prevPass)
                        prevPass = true;
                    else
                        game = false;
                    System.out.println("Black has no valid moves, forced to pass...");
                }
            }

            // Invert 'color' to alternate player turns.
            color = !color;
        }
        System.out.println("\n\nFinal result:\n\n"+gameBoard+"\n");
        if(gameBoard.getPlayerPieceCount(ReversiBoard.PlayerColor.BLACK) > gameBoard.getPlayerPieceCount(ReversiBoard.PlayerColor.WHITE))
            System.out.println("Black player wins.");
        else if(gameBoard.getPlayerPieceCount(ReversiBoard.PlayerColor.BLACK) < gameBoard.getPlayerPieceCount(ReversiBoard.PlayerColor.WHITE))
            System.out.println("White player wins.");
        else
            System.out.println("Tie.");
    }
}
