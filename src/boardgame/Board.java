/* Simple interface class. Describes the absolute basic functions of any theoretical (square) board. */

package boardgame;

public interface Board
{
    public int getWidth();
    public int getHeight();
    public Boolean isSquare();
}
