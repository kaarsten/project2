/* Base class for a player in a board game. */
package boardgame;

public abstract class Player
{
    // Simple boolean descriptor. Returns whether or not this
    // player is a human player. This can only be set at
    // initialization, and is usually set in child classes
    // via the super() call.
    private Boolean human;
    public Player(Boolean Human)
    {
        this.human = Human;
    }
    
    // Return the humanness of this player.
    public Boolean isHuman()
    {
        return this.human;
    }

    // This should be implemented in child classes.
    public abstract void play();
}
