/* 
 * Board piece class. Represents a single piece on a board.
 * NOTE: This is meant to be put into a collection or array
 * of some sort. On its own, it's little more than a placeholder
 * with some metadata.
 * 
 */
package boardgame;

public class Piece
{
    // It is assumed that pieces will have an owner. They store this as a reference
    // to a player object in the class, set at initialization and changeable at
    // runtime via the 'setOwner' class.
    private Player owner;
    private int row,column;
    public Piece(Player owner, int row, int column)
    {
        this.owner = owner;
        this.row = row;
        this.column = column;
    }
    public Player getOwner()
    {
        return this.owner;
    }
    public void setOwner(Player owner)
    {
        this.owner = owner;
    }
    public void setRow(int row)
    {
        this.row = row;
    }
    public void setColumn(int column)
    {
        this.column = column;
    }
    public int getRow()
    {
        return this.row;
    }
    public int getColumn()
    {
        return this.column;
    }
}
