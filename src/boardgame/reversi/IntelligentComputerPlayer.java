package boardgame.reversi;
import boardgame.*;
import java.util.*;

public class IntelligentComputerPlayer extends ComputerPlayer
{
    public IntelligentComputerPlayer(ReversiBoard brd)
    {
        super(brd);
    }
    public void play()
    {
        //throw new IllegalStateException("This is unimplemented!");
		Set<Piece> plays = this.brd.getAvailablePlays(this);
        Piece play = null;
		int flips = 0;
        for (Piece pc : plays)
        {
            if(this.brd.getChanges(this,pc.getRow(),pc.getColumn()).size() > flips)
			{
                flips = this.brd.getChanges(this,pc.getRow(),pc.getColumn()).size();
				play = pc;
			}
        }


        int what=this.brd.makePlay(this, play.getRow(), play.getColumn());
        if (what==-1){
            throw new IllegalStateException("Bad play made!");
        }
        System.out.println("Choosing...");
        waiting(1000);
    }
}
