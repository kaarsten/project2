package boardgame.reversi;
import boardgame.*;

public abstract class ComputerPlayer extends Player
{
    protected ReversiBoard brd;
    public ComputerPlayer(ReversiBoard brd)
    {
        super(false);
        this.brd = brd;
    }
    public static void waiting (int n){
        long t0, t1;
        t0 =  System.currentTimeMillis();
        do{
            t1 = System.currentTimeMillis();
        }while ((t1 - t0) < n );
    }
}
