/*
 * This class represents the empty spots on the Reversi board.
 * This is considered a 'computer' player.
 * NOTE: THIS IS AN INTERNAL PLAYER CLASS. ATTEMPTING TO CALL ITS
 * 'play()' FUNCTION WILL RESULT IN AN EXCEPTION!
 */

package boardgame.reversi;
import boardgame.*;

public class DummyPlayer extends Player
{
    public DummyPlayer()
    {
        super(false);
    }
    public void play()
    {
        throw new IllegalStateException("Neutral player instance should not be called to make a play!");
    }
}
