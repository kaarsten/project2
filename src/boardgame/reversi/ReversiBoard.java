package boardgame.reversi;
import boardgame.*;
import java.util.*;

public class ReversiBoard implements Board
{
    // Various player references.
    private Player neutralPlayer;
    private Player whitePlayer;
    private Player blackPlayer;

    // Board space (2D array of Pieces)
    private Piece board[][];
    
    // Size
    private int size;

    // Two enums describing the valid player types and colors
    // Mostly to restrict inputs from external classes.
    public enum PlayerType
    {
        HUMAN,
        RANDOMCPU,
        SMARTCPU
    }
    public enum PlayerColor
    {
        BLACK,
        WHITE
    }

    // Most basic constructor. Assumes board is default size.
    public ReversiBoard(PlayerType p1, PlayerType p2)
    {
        // Initialize the dummy player and create the two
        // game players.
        neutralPlayer = new DummyPlayer();
        blackPlayer = mkPlayer(p1);
        whitePlayer = mkPlayer(p2);

        // Initialize the board.
        init(8);
    }

    // Same as the previous constructor, but supports custom game
    // board sizes. 
    public ReversiBoard(PlayerType p1, PlayerType p2, int size)
    {
        // Check that the size is valid. If not, throw an exception.
        if(size < 2 || size % 2  == 1)
            throw new IllegalArgumentException( "Invalid size given for Reversi Board" );
        
        // Size is valid, continue as normal and pass the size to the init()
        // function.
        neutralPlayer = new DummyPlayer();
        blackPlayer = mkPlayer(p1);
        whitePlayer = mkPlayer(p2);
        init(size);
    }

    // Utility method to create and return a player based on the passed
    // PlayerType.
    private Player mkPlayer(PlayerType type)
    {
        switch(type)
        {
            case HUMAN:
                return new HumanPlayer(this);
            case RANDOMCPU:
                return new RandomComputerPlayer(this);
            case SMARTCPU:
                return new IntelligentComputerPlayer(this);
        }
        // This really should never happen.
        return null;
    }

    // Utility function to create and populate the board with pieces.
    // Also properly sets up the initial player pieces.
    private void init(int size)
    {
        // Set the board's size variable.
        this.size = size;

        // Create the board space array.
        board = new Piece[size][size];
        
        // Fill the board with 'neutral' (empty) spots.
        for(int r=0; r < size; r++)
            for(int c=0; c < size; c++)
                board[r][c] = new Piece(neutralPlayer,r,c);

        // Place the black and white players' first pieces.
        board[(size/2)][(size/2)].setOwner(blackPlayer);
        board[(size/2)][(size/2)-1].setOwner(whitePlayer);
        board[(size/2)-1][(size/2)-1].setOwner(blackPlayer);
        board[(size/2)-1][(size/2)].setOwner(whitePlayer);
    }

    // Returns a boolean value describing the validity of a
    // given move for a particular player. This is done by checking
    // if the set of changes given for a move is empty (which 
    // indicates an invalid move).
    public Boolean isValidPlay(Player p, int row, int column)
    {
        Set<Piece> changes;
        changes = getChanges(p,row,column);
        return (!changes.isEmpty());
    }

    // Attempts to make a play at a given location for a
    // given player. If successful, return the number of pieces
    // gained. Otherwise, return -1.
    public int makePlay(Player p, int row, int column)
    {
        // Create a set to store the changes.
        Set<Piece> changes;

        // Get the set of changes resulting from the play.
        changes = getChanges(p,row,column);

        // Is it an empty set? The play is invalid, so return -1.
        if(changes.isEmpty())
        {
            return -1;
        }else{
            // Otherwise, this play is valid. Loop through all member
            // pieces in the set and shift the owner appropriately.
            for(Piece pc : changes)
            {
                pc.setOwner(p);
            }
            
            // Return the number of pieces gained.
            return changes.size();
        }
    }

    protected Set<Piece> getChanges(Player p, int row, int column)
    {
        // First thing's first: are the coordinates inside the board's
        // boundaries?
        //
        // Then, is the piece at that spot a neutral (empty) spot?
        if(row >= size || column >= size || row < 0 || column < 0)
        {
            return new HashSet<Piece>();
        }else if(neutralPlayer != getOwnerAt(row,column))
        {
            return new HashSet<Piece>();
        }
        // We apparently have a possibly valid space.
        // Now, begin getting changes.
        
        // Make a set to hold all possible changes (this is
        // probably too big, but who cares, it'll fit for
        // sure.)
        Set<Piece> chng = new HashSet<Piece>(size*size);

        // Make a set to hold all changes in a given direction from
        // the play spot.
        Set<Piece> tmp = new HashSet<Piece>(size);

        // Easy way to loop in all directions. These two for loops
        // will determine how the innermost for loop alters the row
        // and column in each loop. It starts by adding -1 to both, then -1
        // to the row only, then -1 to the row and 1 to the column, and so on...
        //
        // This is a quick and easy way to automate checking in each direction.
        for(int r = -1; r<2; r++)
        {
            for(int c = -1; c<2; c++)
            {
                // First, empty out the temporary array of changes.
                tmp.clear();

                // 'exc' essentially says whether or not a given
                // direction still possibly hold valid changes.
                //
                // 'foundOther' indicates that changes were found
                // in a given direction.
                Boolean exc = true;
                Boolean foundOther = false;

                // Run the loop starting at 1 and continue
                // as long as the changes are possible and
                // not yet found.
                for(int i = 1; exc && !foundOther; i++)
                {
                    // Store a temporary reference to this piece's owner.
                    Player pieceOwner;

                    // Try to retrieve the reference. If it fails, this 
                    // means the loop has exited the boundaries of the board.
                    // This direction cannot hold valid changes, so exit the loop.
                    try{
                        pieceOwner = getOwnerAt(row+(r*i),column+(c*i));
                    }
                    catch(ArrayIndexOutOfBoundsException e)
                    {
                        exc = false;
                        continue;
                    }

                    // Assuming we're checking the spots directly next
                    // to the play coordinates (i=1)
                    if(i == 1 && exc)
                    {
                        // Check that the opponent owns this piece. If it
                        // does not, this direction cannot hold valid changes.
                        if(pieceOwner == p || pieceOwner == neutralPlayer)
                            exc = false;
                        else{
                            // This direction is still possibly valid.
                            // Begin to add pieces to the temporary changes
                            // set
                            tmp.add(board[row+(r*i)][column+(c*i)]);
                        }
                    }
                    else if(exc)
                    {
                        // This is assuming the earest spot was an opponent.
                        // Because of the nature of the loop, we know it will
                        // exit if one of two things happen: a player-owned piece
                        // is found, indicating a valid play, or a neutral (empty)
                        // piece is found, indicating an invalid play.
                        //
                        // If either of these happens, alter the state accordingly.
                        // Otherwise, add the piece (owned by the opponent) to the
                        // temp changes set.
                        if(pieceOwner == neutralPlayer)
                            exc = false;
                        else if(pieceOwner == p)
                            foundOther = true;
                        else
                            tmp.add(board[row+(r*i)][column+(c*i)]);
                    }
                }

                // If the loop exited with changes successfully found, merge
                // them into the main change set.
                if(foundOther)
                    chng.addAll(tmp);
            }
        }

        // If the change set is still empty, no valid changes were found. Return
        // a null set.
        if(chng.isEmpty())
        {
            return new HashSet<Piece>();
        }
        else
        {
            // Valid changes were found. Add the piece itself at the given
            // spot and return the set.
            chng.add(board[row][column]);
            return chng;
        }
    }

    // Returns the availability of plays for a given player
    // (checks if the available plays set is null).
    public Boolean hasAvailablePlays(Player p)
    {
        Set<Piece> plays = getAvailablePlays(p);
        return (!plays.isEmpty());
    }

    // Return a set of available plays for a given player.
    protected Set<Piece> getAvailablePlays(Player p)
    {
        // Essentially, loop through all spots on the board
        // and get the changes if a play is attempted there.
        // Add a spot to the set if it has valid changes.
        Set<Piece> plays = new HashSet<Piece>(size*size);
        for(int r = 0; r < size; r++)
        {
            for(int c = 0; c < size; c++)
            {
                if(getOwnerAt(r,c) == neutralPlayer)
                {
                    Set<Piece> chng = getChanges(p,r,c);
                    if(!chng.isEmpty())
                    {
                        plays.add(board[r][c]);
                    }
                }
            }
        }
        return plays;
    }

    // Get a reference to the Player owning the piece
    // at a given location.
    protected Player getOwnerAt(int row, int column)
    {
        return board[row][column].getOwner();
    }

    // Return the board's squareness.
    public Boolean isSquare()
    {
        return true;
    }

    // Return board width.
    public int getWidth()
    {
        return this.size;
    }

    // Return board height.
    public int getHeight()
    {
        return this.size;
    }

    // Get the number of empty spots.
    public int getFreeSpaceCount()
    {
        return getPPC(neutralPlayer);
    }

    // Get the number of pieces owned by a particular
    // player (Identified by color)
    public int getPlayerPieceCount(PlayerColor c)
    {
        switch(c)
        {
            case BLACK:
                return getPPC(blackPlayer);
            case WHITE:
                return getPPC(whitePlayer);
            default:
                return 0;
        }
    }

    // Internal method used to count pieces owned by a
    // player (denoted by a reference to the player itself).
    private int getPPC(Player p)
    {
        // Loop through all spots and increment count if the
        // piece is owned by the player.
        int count = 0;
        for(int r = 0; r < size; r++)
        {
            for(int c = 0; c < size; c++)
            {
                if(getOwnerAt(r,c) == p)
                {
                    count++;
                }
            }
        }
        return count;
    }

    // Get a reference to a player (denoted by color).
    public Player getPlayer(PlayerColor player)
    {
        switch(player)
        {
            case BLACK:
                return blackPlayer;
            case WHITE:
                return whitePlayer;
            default:
                return null;
        }
    }

    // Special function: Returns a string representation of the board.
    // Prototype:
    // White Pieces (O): N1
    // Black Pieces (X): N2
    // Available spaces (#): N3
    //   1 2 3 4 5 6 7 8
    // 1 # # # # # # # #
    // 2 # # # # # # # #
    // 3 # # # # # # # #
    // 4 # # # O X # # #
    // 5 # # # X O # # #
    // 6 # # # # # # # #
    // 7 # # # # # # # #
    // 8 # # # # # # # #
    public String toString()
    {
        String out =    "Black Pieces (X): " + getPPC(blackPlayer) + "\n" +
                        "White Pieces (O): " + getPPC(whitePlayer) + "\n" +
                        "Available Spaces (#): " + getPPC(neutralPlayer) + "\n" +
                        " ";
        for(int i = 1; i <= size; i++)
        {
            out += " " + i;
        }
        for(int r = 0; r < size; r++)
        {
            out += "\n" + (r+1);
            for(int c = 0; c < size; c++)
            {
                Player own = getOwnerAt(r,c);
                if(own == neutralPlayer)
                    out += " #";
                else if(own == whitePlayer)
                    out += " O";
                else
                    out += " X";
            }
        }
        return out;
    }
}
