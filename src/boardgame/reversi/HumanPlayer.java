/* Human player. Takes input from the console to make plays and gives feedback.
 * This is a fairly basic implementation with error checking. */
package boardgame.reversi;
import boardgame.*;
import java.util.Scanner;

public class HumanPlayer extends Player
{
    // Initialize and provide the reference to the parent board.
    private ReversiBoard brd;
    public HumanPlayer(ReversiBoard brd)
    {
        super(true);
        this.brd = brd;
    }
    public void play()
    {
        // Prompt the user.
        System.out.print("Make your play (enter in 'row,column' format): ");

        // Initialize the scanner and get the next line of input.
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();

        // The input is split at the ','. This should provide 2 strings.
        String components[] = input.split(",");

        // If there are not two strings, there was an invalid number of coordinates
        // given. Notify the user and re-prompt.
        if(components.length != 2)
        {
            System.out.println("Incorrect number count given. Try again.");
            this.play();
            return;
        }

        // We got two strings, so let's convert them to integers.
        // By default, initialize them to -1 (an invalid value).
        int row, column;
        row = column = -1;

        // Attempt conversion. If an exception occurs, notify the
        // user of invalid input and tell them to try again.
        try{
            row = Integer.parseInt(components[0]);
            column = Integer.parseInt(components[1]);
        }
        catch(Exception e)
        {
            System.out.println("Invalid input entered. Please try again.");
            this.play();
            return;
        }

        // We got some sort of number pair, so attempt to make a play with it. Note the
        // subtraction of 1 from the coordinates; the array is indexed beginning at 0,
        // whereas the board shown is indexed beginning at 1, so we adjust accordingly.
        int ret = 0;
        ret = this.brd.makePlay(this, row-1, column-1);

        // If the return value is -1, the play failed. Retry. Otherwise, display the
        // number of pieces won.
        if(ret == -1)
        {
            System.out.println("Invalid play entered. Please try again.");
            this.play();
            return;
        }else
            System.out.println("Play resulted in "+ret+" pieces gained.");
    }
}
