package boardgame.reversi;
import boardgame.*;
import java.util.*;

public class RandomComputerPlayer extends ComputerPlayer
{
    public RandomComputerPlayer(ReversiBoard brd)
    {
        super(brd);
    }
    public void play()
    {
       // throw new IllegalStateException("This is unimplemented!");
   	Set<Piece> plays = this.brd.getAvailablePlays(this);
		int rdm = (int) Math.round(Math.random()*(plays.size()-1));
		Piece play = null;
		int i = 0;
		for (Piece pc : plays)
		{
			if(i==rdm)
				play = pc;
			i++;
		}
		int what=this.brd.makePlay(this, play.getRow(), play.getColumn());
		if (what==-1){
			throw new IllegalStateException("Bad play made!");
		}
        System.out.println("Computer guessing...");
        waiting(1000);
    }
}
