#f
all: obj/Reversi.class

obj/Reversi.class: obj/*/*/*.class
	javac -Xlint:unchecked -classpath obj/ -d obj/ src/Reversi.java

obj/*/*/*.class: obj/*/*.class
	javac -Xlint:unchecked -classpath obj/ -d obj/ src/*/*/*.java

obj/*/*.class: obj/
	javac -Xlint:unchecked -classpath obj/ -d obj/ src/*/*.java

obj/:
	mkdir -pv obj

clean:
	rm -rfv obj

test: all
	./cheater.sh ??art mehy
